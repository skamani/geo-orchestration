FROM inovatrend/tomcat8-java8

RUN apt-get update && apt-get -y install unzip
RUN curl -O -J -L -k http://sourceforge.net/projects/geoserver/files/GeoServer/2.19.2/geoserver-2.19.2-war.zip 
RUN cp geoserver-2.19.2-war.zip /tmp/geoserver.zip
RUN unzip -q /tmp/geoserver.zip -d /tmp
RUN mv /tmp/geoserver.war /opt/tomcat/webapps/geoserver.war

CMD /opt/tomcat/bin/deploy-and-run.sh
